function a = surfacearea(t,p)
% a = surfacearea(t,p)
% 
% Area of the surface defined by the triangulation t of the points p
%
%
% Example:
% p = [0 0 0;1 0 0;0 1 0;1 1 0]; t = [1 2 3; 2 3 4]; surfacearea(t,p)
%
% Jerome Boulanger 2018

a = 1 / 2 * sum(sqrt(sum(cross(p(t(:,2),:)-p(t(:,1),:),p(t(:,3),:)-p(t(:,1),:)).^2,2)));