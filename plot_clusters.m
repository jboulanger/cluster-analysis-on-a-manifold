function [h1,h2,N] = plot_clusters(clusters,membranes,meshes,id)
%  plot_clusters(p,m,edges,idx)
%
% Display clusters on the surface
%
% Jerome Boulanger 2019


i = find(unique(clusters(5,:))==id);
m = membranes(8:10, membranes(5,:) == id)';
p = clusters(8:10, clusters(5,:) == id)';

% plot the initial point cloud
h1 = figure(1);
clf;
plot3(p(:,1),p(:,2),p(:,3),'.');
hold on;
plot3(m(:,1),m(:,2),m(:,3),'.');
hold off
axis equal
legend('proteins','membrane');
xlabel('X [nm]');
ylabel('Y [nm]');
zlabel('Z [nm]');
title(sprintf('Tomogram %d (proteins:%d,membrane:%d)', id, size(p,1), size(m,1)));

h2 = figure(2);
clf;
idx = clusters(21, clusters(5,:) == id);

N = numel(unique(idx));
fprintf('Tomograph %d: mesh:%dptsx%dtri clusters:%d\n', id, size(meshes(i).vertices,1),size(meshes(i).edges,1),N)

% display the points of the membrane
plot3(m(:,1),m(:,2),m(:,3),'.','Color',[0.75,0.75,0.75]);
hold on

% display the surface
trisurf(meshes(i).edges,meshes(i).vertices(:,1), meshes(i).vertices(:,2),meshes(i).vertices(:,3),'facealpha',0.75,'edgealpha',0,'facecolor',[.5 .5 .5],'facelighting','gouraud')

% display the individual clusters with centroids
cmap = lines(max(idx));
for i = 1:max(idx)
    idxi= find(idx==i);
    plot3(p(idxi,1),p(idxi,2),p(idxi,3),'.','MarkerSize',10,'color',cmap(i,:));
    C = mean(p(idxi,:),1);
    plot3(C(1),C(2),C(3),'.','MarkerSize',50,'color',cmap(i,:))
    text(C(1),C(2),C(3),sprintf('%d',i));
end
hold off
axis equal
box on
grid on
shading interp
xlabel('X [nm]');
ylabel('Y [nm]');
zlabel('Z [nm]');
title(sprintf('Tomogram %d (#cluster:%d)', id, N));