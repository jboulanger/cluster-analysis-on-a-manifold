function idx = dbscan_on_mesh(points, vertices, edges, max_distance, min_points)
% p = dbscan_on_mesh(m,p,subsampling)
% 
% Cluster analysis on a surface
%
% points: points to analyse 
% vertices : vertices of the mesh
% edges : edges of the mesh
%
%
% Jerome Boulanger 2018-2019

if nargin < 4
    max_distance = 40;
end

if nargin < 5
    min_points = 4;
end

% compute distance matrix between proteins walking on the membrane
D0 = pdist2(points,vertices); % min from proteins to membrane
J = edges(:,[2 3 1]); E = [edges(:) J(:)]; % compute the edges
[costs, paths] = dijkstra(vertices,E); % compute shortest pathes   
% compute the distance matrix for the proteins
D = zeros(size(points,1),size(points,1)); 
for k = 1:size(points,1) 
    [~,s] = min(D0(k,:)); % map the protein k to the membrane    
    for l = k+1:size(points,1)
        [~,d] = min(D0(l,:)); % map the protein l to the membrane
        D(k,l) = costs(s,d); % get the length of the path from s to d
        D(l,k) = costs(d,s);
    end       
end

% cluster analysis using dbscan
[idx,isnoise] = DBSCAN(points,max_distance,min_points,D);

