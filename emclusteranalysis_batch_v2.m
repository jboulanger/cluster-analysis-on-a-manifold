% clustering analysis of proteins on a membrane from EM tomography data
clear all; close all
% select the membrane file
%[fm,dm] = uigetfile('../../data/rene/clustering/*.txt','Select the membrane file');
%[fp,dp] = uigetfile('../../data/rene/clustering/*.txt','Select the protein file');
%tomonum=5;

%%  inputs %%
membrane_file = emread('/net/bstore1/bstore1/publicshare/forRene/manualpick/subtomo/combinedmotl/allmotlposm_1.em');
pos_file = emread('/net/bstore1/bstore1/publicshare/forRene/manualpick/subtomo/combinedmotl/allmotlclass_1.em');
savepath = '';  %path were plots are going to be saved
%%

tomos=unique(pos_file(5,:));

for i=1:length(tomos)
	tomo_mem = membrane_file(:,membrane_file(5,:)==tomos(i));
	tomo_pos = pos_file(:,pos_file(5,:)==tomos(i));
	tomonum = tomos(i);
	emclusteranalysis_func(tomo_mem,tomo_pos,tomonum,savepath)
end


function emclusteranalysis_func(membrane_file,pos_file,tomonum,savepath)	

%% load the data
%m = dlmread([dm fm],'\t');
%p = dlmread([dp fp],'\t');
m = membrane_file(8:10,:)';
p = pos_file(8:10,:)';

p(:,3) = p(:,3) - mean(p(:,3)) + mean(m(:,3));
fprintf('Membrane file %d with %d points.\n', tomonum, size(m,1));
fprintf('Protein file %d with %d points.\n', tomonum, size(p,1));
%% display the proteins and the membrane coordinates
f=figure;
figure(f)
plot3(p(:,1),p(:,2),p(:,3),'.');
hold on;
plot3(m(:,1),m(:,2),m(:,3),'.');
hold off
axis equal
saveas(gcf,[savepath,'./surface_tomo',num2str(tomonum),'.fig']);
%%
% compute distance matrix between proteins walking on the membrane
tic;
subsampling = 100;
ms = m(1:subsampling:end,1:3);
plot3(ms(:,1),ms(:,2),ms(:,3),'.');axis equal
D0 = pdist2(p,ms); % min from proteins to membrane
D1 = pdist2(ms,ms); % distance matrix inside the membrane
I = double(MyCrustOpen(ms));
trisurf(I,ms(:,1),ms(:,2),ms(:,3));%,'edgealpha',0,'facecolor',[.7 .5 .5],'facelighting','gouraud')
%I = delaunayn(ms); % create a triangulation
J = I(:,[2 3 1]); E = [I(:) J(:)]; % compute the edges
[costs,paths] = dijkstra(ms,E); % compute shortest pathes   
% compute the distance matrix for the proteins
D = zeros(size(p,1),size(p,1)); 
for k = 1:size(p,1);    
    [~,s] = min(D0(k,:)); % map the protein k to the membrane    
    for l = k+1:size(p,1);            
        [~,d] = min(D0(l,:)); % map the protein l to the membrane
        D(k,l) = costs(s,d); % get the length of the path from s to d
        D(l,k) = costs(d,s);
    end       
end
toc
%% cluster analysis using dbscan
max_distance = 40;
min_points = 4;
[idx,isnoise] = DBSCAN(p,max_distance,min_points,D);
fprintf('The are %d clusters in tomogram %d.\n', max(idx),tomonum);
% display the cluster and the points on the membrane used in the shortest
% path computations.
h=figure;
figure(h)
cmap = lines(max(idx));
for i = 1:max(idx);
    idxi= find(idx==i);
    plot3(p(idxi,1),p(idxi,2),p(idxi,3),'.','MarkerSize',10,'color',cmap(i,:));
    C = mean(p(idxi,:),1);        
    hold on
    plot3(C(1),C(2),C(3),'.','MarkerSize',50,'color',cmap(i,:))
    text(C(1),C(2),C(3),sprintf('%d',i));
    for k = 1:size(idxi,1);        
        [~,s] = min(D0(idxi(k),:));
        for l = 1:size(idxi,1);             
            [~,d] = min(D0(idxi(l),:));
            r = paths{s,d};
            plot3(ms(r,1),ms(r,2),ms(r,3),'-','color',cmap(i,:));              
        end        
    end    
end
plot3(p(isnoise,1),p(isnoise,2),p(isnoise,3),'k.','MarkerSize',10);
plot3(ms(:,1),ms(:,2),ms(:,3),'.','Color',[0.75,0.75,0.75]);
hold off
axis equal
box on
grid on
saveas(gcf,[savepath,'./surface_cluster_tomo',num2str(tomonum),'.fig']);
end

