function p = register_pointclouds(p,m)
% p = register_pointclouds(m,p)
%
% Align poin clouds along z direction
% align p to m
%
% Jerome Boulanger 2018-2019

p(:,3) = p(:,3) - mean(p(:,3)) + mean(m(:,3));