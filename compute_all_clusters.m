function [clusters, meshes] = compute_all_clusters(proteins, membranes, nmeshsamples)
% [clusters, meshes] = compute_all_clusters(proteins, membranes, nmeshsamples)
% [clusters, meshes] = compute_all_clusters(proteins, membranes)
%
% Cluster analysis on mesh for all tomogrames
%
% Input:
% proteins :  coordinates of the points to cluster (as read by emread)
% proteins :  coordinates of the points defining the membrane (as red by emread)
% nmeshsamples : (optional) number of samples for sub-sampling (use for debuging)
%
% Output
% clusters : array to be save with emwrite with the 21st column as a
%            cluster index
% meshes : an array of structs edges and vertices defining the meshes
% 
% Jerome Boulanger 2019

if nargin < 3
    nmeshsamples = -1;
end

tic
tomos = unique(proteins(5,:)); %  determine the list of individual tomograms
% allocate memory
meshes(numel(tomos)).edges = 0;
clusters =  [proteins; -ones(1,size(proteins,2))];
% for each tomograms
for i = 1:length(tomos)    
    id = tomos(i);    
	m = membranes(8:10, membranes(5,:) == id)';
	p = proteins(8:10, proteins(5,:) == id)';
    fprintf('Tomogram %d/%d [id:%d] membrane:%d proteins:%d\n', i, length(tomos), id, size(m,1),size(p,1));
    err = 1;
    ntrial = 0;
    while err == 1 && ntrial < 10
        try 
            err = 0;  
            % sub sampling by 50%
            N = round(50*size(m,1)/100);
            stride = unique(ceil(size(m,1)*rand(N,1)));
            m = m(stride,:);
            p = register_pointclouds(p,m);
            e = double(MyCrustOpen(m));       
            if nmeshsamples > 1 % sub sampling                
                [e,m] = reducepatch(e,m,nmeshsamples/100);
            end
            meshes(id).edges = e;
            meshes(id).vertices = m;            
            c = dbscan_on_mesh(p,m,meshes(id).edges);            
            clusters(21, clusters(5,:) == id) = c; 
            clusters(10, clusters(5,:) == id) = p(:,3); 
        catch  em
            fprintf(' :: analysis of tomogram %d failed (try %d/10)\n', i, ntrial);            
            err = 1;
            ntrial = ntrial + 1;            
        end
        if ntrial == 10
            warning('Unable to process tomogram %d', i);            
        end
    end    
end
toc