% Clustering analysis of proteins on a membrane from EM tomography data
% 
% 
% The file is composed of 3 sections: set parameters, analysis, rendering
%
%
% 30 jan 2019  - Separate computation and figures + write em file
% 
% Jerome Boulanger 2018-2019

clear all; close all
% addpath('/lmb/home/rawf2/');startup; % run this once

%  inputs files
membranes = emread('/net/bstore1/bstore1/publicshare/forRene/manualpick/subtomo/combinedmotl/allmotlposm_1.em');
proteins = emread('/net/bstore1/bstore1/publicshare/forRene/manualpick/subtomo/combinedmotl/allmotlclass_1.em');

% output directory and files
opath = 'tmp';  % output path
cemfile = 'clusters'; % em file with clusters label in column 21
pfigfile = 'surface_tomo'; % base name of the .fig file  with points clouds
cfigfile = 'surface_cluster_tomo'; % base name of the .fig file with clusters
meshfile = 'mesh'; % name of the .mat file for the mesh

% downsample parameter the membrane coordinates to speed up processing
% the value is in percentage of the original number of points
nmeshsamples = 20; 

if ~exist(opath,'dir')
    mkdir(opath);
end

%% compute all clustering
tic
[clusters, meshes] = compute_all_clusters(proteins, membranes, nmeshsamples);
toc
emfilename = sprintf('%s%c%s.em', opath, filesep, cemfile);
fprintf('Saving clusters in em file ''%s'' \n', emfilename);
emwrite(clusters, emfilename);

meshfilename = sprintf('%s%c%s.mat', opath, filesep, meshfile);
fprintf('Saving meshes in mat file ''%s'' \n', meshfilename);
save(meshfilename,'meshes');


%% save all figures
% To reload previously saved data:
% clusters = emread(emfilename);
% meshes  = load(meshfilename);
tomos = unique(clusters(5,:));
Nclusters = zeros(1,numel(tomos));
for t = 1:numel(tomos)
    id = tomos(t);    
    [h1,h2,Nclusters(t)] = plot_clusters(clusters,membranes,meshes,id);
    pfilename = sprintf('%s%c%s.fig', opath, filesep, pfigfile);
    saveas(h1, pfilename);
    cfilename = sprintf('%s%c%s.fig', opath, filesep, cfigfile);
    saveas(h2, cfilename);
end
%%
figure(3);
bar(tomos,Nclusters); xlabel('Tomograpm id');ylabel('number of clusters');
title('Number of clusters by tomograpm')

