function plot_pointcloud(p,m)
plot3(p(:,1),p(:,2),p(:,3),'.');
hold on;
plot3(m(:,1),m(:,2),m(:,3),'.');
hold off
axis equal
legend('proteins','membrane');
xlabel('X [nm]');
ylabel('Y [nm]');
zlabel('Z [nm]');
